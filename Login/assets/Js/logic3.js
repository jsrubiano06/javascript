const usuario = document.getElementById('usuario');
const cerrar = document.getElementById('cerrar');

let usuarioensesion = JSON.parse(localStorage.getItem('user'));

if (usuarioensesion != null) {
  usuario.innerHTML =
    `<a id="cerrar">` + usuarioensesion[0].usuario +`</a>`;
} else {
  usuario.innerHTML = alert("Inciar sesión Por favor")
}
cerrar.addEventListener('click', function () {
  localStorage.clear('user');
  location.href = '../../index.html';
});

//user = nombre de mi localstorage