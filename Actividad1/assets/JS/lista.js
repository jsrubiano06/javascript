const btn = document.getElementById('btn');
const lista = document.getElementById('lista')
const mensajeAlerta = document.getElementById('mensajeAlerta')
const user = document.getElementById('user')
let usuarios = [];
let existe;

 btn.addEventListener('click', ()=> {
    const cajaTexto = document.getElementById('nombre');
    const nombre = cajaTexto.value;
    if (nombre != "") {
        existeUsuario(nombre);
            if (!existe) {
                usuarios.push(nombre);
                mensajeAlerta.classList.add('ocultar')

            }
            else {
                user.innerText = nombre
                mensajeAlerta.classList.remove('ocultar')
            }
    } else {
        usuarios.sort();
        pintarArray();
    }
     cajaTexto.value = ""
    
 });
 const pintarArray = () => {
     for (usuario of usuarios) {
         let etiqueta = document.createElement("li");
         etiqueta.textContent = usuario;
         lista.appendChild(etiqueta)
     }
 }
 const existeUsuario = (userIn) => {
     existe = false;
       for (usuario of usuarios) {
           if (userIn === usuario)
             existe=true;
       }
 }
