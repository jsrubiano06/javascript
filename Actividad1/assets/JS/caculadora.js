const caluladora = (operacion) =>{
    let num1 = document.getElementById("valor1");
    let num2 = document.getElementById("valor2");
    let valor1 = parseInt(num1.value);
    let valor2 = parseInt(num2.value);

    let resultado  = document.getElementById("resultado");

    let rta;

    switch(operacion){
        case 'suma':
          rta = valor1 + valor2
          break;
        case 'resta':
          rta = valor1 - valor2
          break;
        case 'multiplica':
          rta = valor1 * valor2
          break;
        case 'divide':
          rta = valor1 / valor2
          break;
        case 'limpia':
          num1.value=""
          num2.value=""
          rta = "";
          break;        
      default:
          break;
    }
    resultado.innerHTML = rta
}